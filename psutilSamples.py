#!/usr/bin/python3

import psutil 
import platform as plf
from datetime import datetime

class psutilSample:


    @classmethod
    def ptp(cls):

        """ Display network interfaces that use point-to-point interfaces (VPNs can be included) """

        if plf.system() in ("Darwin", "Linux"):

            [[print(key, "#" , {"address": value[0].address, "netmask": value[0].netmask, "ptp": value[0].ptp, "family": int(value[0].family)}) if value[0].ptp else None] for key, value in psutil.net_if_addrs().items()]



    @classmethod
    def broadcast(cls):

        """ Display network interfaces that use broadcast """
        if plf.system() in ("Darwin", "Linux"):

            [[print(key, "#" , {"address": value[0].address, "netmask": value[0].netmask, "broadcast": value[0].broadcast, "family": int(value[0].family)}) if value[0].broadcast and value[0].netmask else None] for key, value in psutil.net_if_addrs().items()]



    @classmethod
    def openfiles(cls):

        """ Lists only processes that have opened files """

        if plf.system() in ("Darwin", "Linux"):

            for process in psutil.process_iter():

                process = process.as_dict(attrs=["cmdline", "create_time", "name", "username", "open_files"])

                try:
                    if process["open_files"]:

                        print("Name         :", process["name"])
                        print("Create Time  :", process["create_time"], "=>", datetime.fromtimestamp(process["create_time"]))
                        print("Username     :", process["username"])
                        print("Command Line :\n\t" + " " * 6, " ".join(list(map(str, process["cmdline"]))), "\n")
                        print("Open Files   :")
                        [print("\t" + " " * 6, openfile.path) for openfile in process["open_files"]]

                        print("\n" + "-" * 100 + "\n")
                except:
                    pass

    

    @classmethod
    def process_user(cls, user):

        if isinstance(user, str):

            for process in psutil.process_iter():

                process = process.as_dict()

                if process["username"] == user:

                    if plf.system() in ("Darwin"):

                        try:
                            [print(process["name"], "#", datetime.fromtimestamp(p["create_time"])) if process["environ"]["XPC_FLAGS"] == "0x0" else None]
                        except:
                            pass

    
    
    @classmethod
    def process_connections(cls):

        if plf.system() in ("Darwin", "Linux"):

            for process in psutil.process_iter():

                """ List only processes that use network connections """

                process = process.as_dict(attrs=["name", "create_time", "username", "connections"])

                process_connections = process["connections"]

                command_filters = (
                    process_connections,
                    process_connections and process_connections[0].raddr and process_connections[0].laddr.ip not in ("0.0.0.0", "127.0.0.1"),
                    process_connections and process_connections[0].raddr,
                    process_connections and process_connections[0].raddr and process_connections[0].raddr.port in (443, 80),
                    process_connections and (int(process_connections[0].family) == 2 if process_connections[0].family else ""), # 2 => IPv4
                    process_connections and (int(process_connections[0].family) == 30 if process_connections[0].family else ""), # 30 => IPv6
                )

                if command_filters[1]:

                    print("Name         :", process["name"])
                    print("Create Time  :", datetime.fromtimestamp(process["create_time"]))
                    print("Username     :", process["username"])
                    print("Connections  :")
                    print("\t" + " " * 7 + "FAMILY" + (" " * 2)  + "LADDR" + ("\t" * 5 + " ") + "LADDR" + (" " * 3) + "RADDR" + ("\t" * 5 + " " * 3) + "RADDR" + (" " * 3) + "STATUS")
                    print("\t" * 2 + (" " * 7) + "IP" + ("\t" * 5 + " ") + "PORT" + (" " * 4) + "IP" + ("\t" * 5 + " " * 3) + "PORT", "\n")

                    for connection in process_connections:

                        data = {}

                        data["family"] = {2: "IPv4", 30: "IPv6"}[int(connection.family)] if connection.family else ""
                        data["laddr_ip"], data["laddr_port"] = (connection.laddr.ip, connection.laddr.port) if connection.laddr else ("", "")
                        data["raddr_ip"], data["raddr_port"] = (connection.raddr.ip, connection.raddr.port) if connection.raddr else ("", "")
                        data["status"] = connection.status if connection.status else ""

                        print("\t" + " " * 6, (" " * 3).join([data[0].ljust(data[1]) for data in zip(list(map(str, data.values())), (5, 39, 5, 39, 5, 11))]))

                    print("\n" + "-" * 137 + "\n")



    @classmethod
    def user_connected(cls):

        """ Lists users who are currently connected """

        if plf.system() in ("Darwin", "Linux"):

            print("Connected users :\n")
            [print("\t", user.name, "#", datetime.fromtimestamp(user.started), "=>", (datetime.now() - datetime.fromtimestamp(user.started))) for user in psutil.users()]


print("\n# " + "-" * 50 + " .::", datetime.now(), "::. " + "-" * 50, " #\n")
psutilSample.user_connected()